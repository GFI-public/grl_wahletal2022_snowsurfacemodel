# GRL_Wahletal2022_snowsurfacemodel
current version: 2.0 (November 2022)

The model code that was used to generate the results published in Geophysical Research Letters:
"Atmosphere-Snow Exchange Explains Surface Snow Isotope Variability" by Wahl et al. 

The functions for the snow surface model are provided in smf_publish.py. In SnowSurfaceModel_run.py I provide an exemplary model code how the model can be run. As the model is run with observational data, it needs to be adapted to the user's needs and observational data format before it can produce results.

How to cite:

The Zenodo Concept doi to cite this software is 10.5281/zenodo.7117641
The code is published under the GNU GPLv3 licence as specified in the licence file. 





#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May  9 17:09:51 2021

a compilation of all the functions needed for the snow surface model runs

especially the sensitivity studies 


@author: sonjaw
"""

import numpy as np
import typhon.physics as ty #very useful python package for unit conversions



def calc_z0q(z0m,**kwargs):#
    """ Andreas 1987: This function estimates the roughness length for humidity (z0q) from using
        the roughness length for momentum (z0m) 
        z0m= momentum roughness length [m]
        provide either:
        z= measurement height m
        WS= average wind speed at measurement height z in m/s
        rho_air= average air density in kg/m3 
        OR
        u_star from observations [m/s]
    """
    vis=1.46*1e-5 #kinemativ viscotiy of air in m2 s-1
    # all following formulas are taken from Andreas 1987
    # approximate u_star from bulk measurements, OR take it from observations  
    #if you do not have u_star observations: In a general way, u∗ can be expressed as a fraction of the average wind speed <u>, i.e. u_star=<u>/N where the index of 
    #roughness N varies from 3 for perfectly smooth surfaces, such as snow or a calm lake surface, to 13 for grassy land. (Camuffo 2014, eq 7.1)
    if 'u_star' in kwargs.keys():
        u_star=kwargs['u_star']
        Re_star=u_star*z0m/vis
    elif set(['z','WS','rho_air']).issubset(kwargs.keys()):    #calculate from bulk observations
        z=kwargs['z']
        WS=kwargs['WS']
        rho_air=kwargs['rho_air']
        k=0.4 # van Karman constant
        C_D= k**2/(np.log(z/z0m))**2#drag coefficient
        tau= rho_air*C_D*WS**2  #surface stress
        u_star=(tau/rho_air)**(0.5)
        Re_star=u_star*z0m/vis
        print(Re_star.shape)
    else:
        print('Error: neither u_star nor z,WS,rho_air parameter provided')
        return
    try: #vector
        z0q=np.full(len(Re_star),np.nan)
        mask_smooth=(Re_star<0.135)
        b0, b1, b2 = [1.610, 0, 0] # following parameters for smooth regime (Re_star<0.135) Tbale 1 Andreas 1987 for z0q
        z0q[mask_smooth]=(z0m*np.exp(b0+b1*np.log(Re_star)+b2*np.log(Re_star)**2))[mask_smooth]

        mask_inter=(Re_star>0.135) & (Re_star<2.5)
        b0, b1, b2 = [0.351,-0.628,0] # following parameters for transition regrime Tbale 1 Andreas 1987 for z0q  
        z0q[mask_inter]=(z0m*np.exp(b0+b1*np.log(Re_star)+b2*np.log(Re_star)**2))[mask_inter]

        mask_rough=(Re_star>2.5)
        b0, b1, b2 = [0.396, -0.512,-0.180] # following parameters for rough regrime Tbale 1 Andreas 1987 for z0q
        z0q[mask_rough]=(z0m*np.exp(b0+b1*np.log(Re_star)+b2*np.log(Re_star)**2))[mask_rough]
    except: #single value
        if Re_star<0.135:
            b0, b1, b2 = [1.610, 0, 0]
        elif (Re_star>0.135) & (Re_star<2.5):
            b0, b1, b2 = [0.351,-0.628,0]
        else:
            b0, b1, b2 = [0.396, -0.512,-0.180]
        z0q=(z0m*np.exp(b0+b1*np.log(Re_star)+b2*np.log(Re_star)**2))
    return z0q

def f_gg_ice(t):
    '''calculate saturation vapour pressure over ice in Pa from Tmpearture °C Goff, J. A. and S. Gratch, 1946'''
    
    t = t + 273.15
    
    if np.any(t <= 0.0):
        print('ERROR: f_gg_ice given temperature below Abs Zero')
        vp_ice = np.nan
    
    
    if np.any(t > 273.15):
        print('ERROR: f_gg_ice given temperature above freezing')
        vp_ice = np.nan
    
    
    log_ten_sp = np.log10(1013.25)     #steam point vapour pressure
    
    ratio = 273.16/t
    wk1 = -9.09685*(ratio-1.0)
    wk1 = wk1 - 3.56654*np.log10(ratio)
    wk1 = wk1 + 0.87682*(1.0 - (1/ratio))
    wk1 = wk1 + log_ten_sp                     #adjusted for Pa      
    wk1 = wk1 - 2.2195983
    wk2 = 10**wk1                              # antilog      
    vp_ice = wk2 * 100.0
    return vp_ice

def f_gg_inv_ice(es_ice):
    '''Goff, J. A. and S. Gratch, 1946'''
    ''' Temperature from saturation vapour pressure over ice,
    es_ice probably in Pa'''            
    P = [1.7252470e-004,2.2522915e-004,9.9097417e-003,1.0602545e-001,1.3392810e+000,1.6931160e+001,-6.0566980e+001]
    
    les_ice = np.log10(es_ice)
    T = np.polyval(P,les_ice)
    return T



# fractionation factor equations
def alphaD_MN(T):
    ''' fractionation factor for Deuterium alpha_ice/vapour (Merlivat & Nief 1967)
    -40°C bis 0°C
    verg. auch "HC notes on fractionation" in Mendeley:'''
    '''the third one is the one abby used, but I do not know where she gets it from'''
    if np.any(T>0):
        print('ERROR alpha: Temp above 0 °C')    
    # for alpha_vapour/ice simply 1/alphaD
    TK=T+273.15
    alphaD=np.exp(-94.5*1e-3+(16.289*1e3/(TK**2))) #also given in MN paper on page 125
    return alphaD

def alpha18O_Maj(T):
    ''' fractionation coefficient for 18O alpha_ice/vapour (Majoube 1971b):
    from 0°C- -33.4°C,  Temp in K'''
    if np.any(T>0):
        print('ERROR alpha: Temp above 0 °C')
    #T in K 
    # for alpha_vapour/ice simply 1/alpha18O
    TK=T+273.15
    alpha18O=np.exp((11.839/TK)-28.224*1e-3)
    return alpha18O



# define model functions
def deltaF(h,k,delta_v,delta_s,Ts):
    #sublimation mode
    # this is the Merlivat & Jouzel 1979 approach:
    # it should detect aoutomatically if its D or 18O data:
    # h = absolute value (not %), relative humidity normalized to surface Temp
    # Ts= °C, surface Temperature
    # k = absolute value (not ‰), kinetic fractionation coefficient
    # delta_v= ‰, isotope composition of air 
    # delta_S= ‰, isotope composition of top snow layer
    if delta_v<-100:  #this is deltaD then
        deltaF=(((1-k)/(1-h))*(1/alphaD_MN(Ts)*(delta_s*1e-3+1)-h*(delta_v*1e-3+1))-1)*1e3
    else: #d18O
        deltaF=(((1-k)/(1-h))*(1/alpha18O_Maj(Ts)*(delta_s*1e-3+1)-h*(delta_v*1e-3+1))-1)*1e3
    return deltaF



def alpha_RMK(h,Ts,D_D):
    # alpha_s/v=Rs/Rv (s for solid)
    # calculate kinetic fractionation coefficient for supersaturation conditions (for deposition)
    # after Jouzel and Merlivat 1984
    # h = supersaturation (Si=Pw/Pwsat(Tsurf)) in absolute values (not in %), has to be >1, can be calucalted with calc_Si.. values up to 1.2 seem reasonable
    # Ts = °C, surface Temperature
    # D_D = diffusivity difference, for HDO=0.9755, for H218O=0.9723 (this defines which equilibrium equation is used)
    
    if h<1:
        print('saturation not reached')
        return
    if D_D==0.9755: # Deuterium
        alpha_RMK=alphaD_MN(Ts)*h/(alphaD_MN(Ts)*1/D_D*(h-1)+1)
        return alpha_RMK
    elif D_D==0.9723: # 18O
        alpha_RMK=alpha18O_Maj(Ts)*h/(alpha18O_Maj(Ts)*1/D_D*(h-1)+1)
        return alpha_RMK
    else:
        print('wrong diffusion ratio')
        return

  

def snow_profile_ini(date,dfST,resolution,season=2019):
    H=5*1e-2 #5cm in m
    n=H/resolution
    
    if (n==10.) | (n==20.): #5mm or 2.5mm
        d18O_s=np.zeros([10])
        dD_s=np.zeros([10])


        if season==2019:
            d18O_s[0]=dfST.loc[date,'d18O_05cm']
            d18O_s[1]=dfST.loc[date,'d18O_05to1cm']
            d18O_s[2]=dfST.loc[date,'d18O_1to2cm']
            d18O_s[3]=dfST.loc[date,'d18O_1to2cm']
            d18O_s[4]=dfST.loc[date,'d18O_2to5cm']
            d18O_s[5]=dfST.loc[date,'d18O_2to5cm']
            d18O_s[6]=dfST.loc[date,'d18O_2to5cm']
            d18O_s[7]=dfST.loc[date,'d18O_2to5cm']
            d18O_s[8]=dfST.loc[date,'d18O_2to5cm']
            d18O_s[9]=dfST.loc[date,'d18O_2to5cm']


            dD_s[0]=dfST.loc[date,'dD_05cm']
            dD_s[1]=dfST.loc[date,'dD_05to1cm']
            dD_s[2]=dfST.loc[date,'dD_1to2cm']
            dD_s[3]=dfST.loc[date,'dD_1to2cm']
            dD_s[4]=dfST.loc[date,'dD_2to5cm']
            dD_s[5]=dfST.loc[date,'dD_2to5cm']
            dD_s[6]=dfST.loc[date,'dD_2to5cm']
            dD_s[7]=dfST.loc[date,'dD_2to5cm']
            dD_s[8]=dfST.loc[date,'dD_2to5cm']
            dD_s[9]=dfST.loc[date,'dD_2to5cm']

        elif season==2018:
            d18O_s[0]=dfST.loc[date,'d18O_05cm']
            d18O_s[1]=dfST.loc[date,'d18O_05to1cm']
            d18O_s[2:]=dfST.loc[date,'d18O_1to2cm']


            dD_s[0]=dfST.loc[date,'dD_05cm']
            dD_s[1]=dfST.loc[date,'dD_05to1cm']
            dD_s[2:]=dfST.loc[date,'dD_1to2cm'] 

        else:
            print('select valid season')
            return
    elif n==5.: # 1cm resolution
        d18O_s=np.zeros([5])
        dD_s=np.zeros([5])        
        
        if season==2019:
            d18O_s[0]=dfST.loc[date,'d18O_1cm']
            d18O_s[1]=dfST.loc[date,'d18O_1to2cm']
            d18O_s[2]=dfST.loc[date,'d18O_1to2cm']
            d18O_s[3]=dfST.loc[date,'d18O_2to5cm']
            d18O_s[4]=dfST.loc[date,'d18O_2to5cm']

            dD_s[0]=dfST.loc[date,'dD_1cm']
            dD_s[1]=dfST.loc[date,'dD_1to2cm']
            dD_s[2]=dfST.loc[date,'dD_1to2cm']
            dD_s[3]=dfST.loc[date,'dD_2to5cm']
            dD_s[4]=dfST.loc[date,'dD_2to5cm']

        elif season==2018:
            d18O_s[0]=dfST.loc[date,'d18O_1cm']
            d18O_s[1:]=dfST.loc[date,'d18O_1to2cm'] 

            dD_s[0]=dfST.loc[date,'dD_1cm']
            dD_s[1:]=dfST.loc[date,'dD_1to2cm'] 

        else:
            print('select valid season')
            return
    else:
        print('select either 0.25cm, 0.5cm or 1cm')
        return        
    
    if n==20.: #2.5mm resolution
        d18O_s=np.repeat(d18O_s, 2)  
        dD_s=np.repeat(dD_s, 2)
    return d18O_s, dD_s



# __Johnsen-Diffusion Model__
def calc_omegafirn(T,P,rho_s):
    '''calculate isotopic diffusivity in firn according to Johnsen (2000) model using the proposed way to calculate saturation vapor pressure and but using my functions for fractionation factors
    input: 
        T= Temperature of firn in °C
        P= Pressure at site in hPa (original is atm)
        rho_s = density of snow kg/m3
    output: 
        omega_18O, omega_D in m2/s'''
    T0=273.15 #0°C in Kelvin
    TK=T+T0 #absolute temperature in Kelvin
    P0=1013.25 #hPa
    rho_ice=917 # kg/m3

    # calculate leading coefficient (mP/RT):
    molarwt = 18.015/1000 # molar weight of water (kg/mol)
    Rgas = 8.314 # J/mol-K  %% gas constant
    Psat = f_gg_ice(T)#3.454e12 * np.exp(-6133/TK) # saturation vapor pressure over ice in Pascals;  formula used by Johnsen
    mPRT = molarwt*Psat/(Rgas*TK) # final values for leading coefficient (mP/RT):

    # calculate diffusivity of water vapor in open air (Omega_a)
    # Johnsen method (Eq.19 Johnsen2000; from Hall and Pruppacher 1976)
    D_18O = 1.0285 #diffusivity of O18
    D_D = 1.0251 #diffusivity of D 
    Omega_air = 0.211 * (TK/T0) ** 1.94  * (P0/P) # Diffusivity of water vapor in air, in cm^2/s, accurance 5-10% 
    Omega_air = Omega_air * 1e-4 #m^2/s

    # isotopic diffusivity in air (m^2 / s)
    Omega_air18 = Omega_air/D_18O #diffusivity of 18O in air
    Omega_airD = Omega_air/D_D #diffusivity of D in air

    # tortuosity factor
    b = 1.3 #constant
    tauinv = 1-b*(rho_s/rho_ice)**2 

    # isotopic diffusivity in firn (m^2 / s)
    omega_18O = mPRT*Omega_air18*tauinv/alpha18O_Maj(T)*((1/rho_s)-(1/rho_ice)) #use Majoube Maj
    omega_D = mPRT*Omega_airD*tauinv/alphaD_MN(T)*((1/rho_s)-(1/rho_ice)) #use Merlivat and Nief MN
    return omega_18O, omega_D

#%
def calc_psi(z_L,y=15): #  #Paulson 1970
    """ function that takes a Series as input
    z_L=stability parameter # make a version of function that calculates stability parameter from gradients
    y= choose a y from within range [11-20], 15 is used if not specified otherwise which is empirical from Dyer 1967"""
    if np.size(z_L)==1:
        #for stable conditions, --> Holtslag and De Bruin 1988
        #if (z_L>0.5)|(z_L<-1): 
        #    psim=np.nan
        #    psih=np.nan
        if (z_L>0.5): #instead of setting to nan (which would result in nan in h and interpolation does no work either)
            z_L=0.5
        if (z_L<-1):
            z_L=-1
        
        if z_L>0:
            psim=-5*z_L  #for z_L < 0.5 (which is extremely stable already)
            psih=psim
        # for unstable conditions Paulson 1970    
        elif z_L<=0:
            x=(1-y*z_L)**(0.25)
            psim=2*np.log((1+x)/2)+np.log((1+x**2)/2)-2*(1/np.tan(x))+np.pi/2  #psi1
            psih=2*np.log((1+x**2)/2)
    else:
        z_Lw=z_L.copy() #
        psim=np.ones(len(z_Lw))*np.nan
        psih=np.ones(len(z_Lw))*np.nan
        x=np.ones(len(z_Lw))*np.nan
        #for stable conditions, --> Holtslag and De Bruin 1988
        
        z_Lw[z_Lw>0.5]=0.5 #instead of setting them nan.. set them to stable condition and unstable condition 
        z_Lw[z_Lw<-1]=-1
        
        mask_stable=((z_Lw>0)&(z_Lw<=0.5))
        psim[mask_stable]=-5*z_Lw[mask_stable]  #for z_L < 0.5 (which is extremely stable already)
        psih=psim
        # for unstable conditions Paulson 1970    
        mask_unstable=((z_Lw<=0)&(z_Lw>=-1))
        x[mask_unstable]=(1-y*z_Lw[mask_unstable])**(0.25)
        psim[mask_unstable]=2*np.log((1+x[mask_unstable])/2)+np.log((1+x[mask_unstable]**2)/2)-2*(1/np.tan(x[mask_unstable]))+np.pi/2  #psi1
        psih[mask_unstable]=2*np.log((1+x[mask_unstable]**2)/2)
    return psim, psih



#%%  THIS IS THE SURFACE Snow model code
#    this function if run for every single day-to-day period and estimates the error bars

def MCruns(runs,d18Osini,dDsini,dexsini,LEini,vd18Oini,vdDini,qini,ustar,zl,TempS,Pa,rho_s,rho_a,z0,z_q,dtstep,resolution,linear_deposition=True,equilibrium_sublimation=False,VapExchange=True,returnarray=True,disturbance=True): #all input in array format
    '''
    runs: specify integer. For Monte Carlo runs (model uncertainty) use 1000
    d18Osini: initial snow isotopic composition profile d18O in ‰
    dDsini: initial snow isotopic composition profile dD in ‰
    dexsini: initial snow isotopic composition profile dex in ‰
    vd18Oini: vapor isotopic composiiton timeseries d18O in ‰
    vdDini: vapor isotopic composiiton timeseries dD in ‰
    qini: humidity timeseries specific humidity g/kg
    ustar: friction velocity timeseries m/s
    zL: atmospheric stability parameter
    TempS: snow temperature timeseries °C
    Pa: ambient pressure in Pascall
    rho_s: snow density kgm-3
    rho_a: air density kgm-3
    z0: surface roughness length in m
    z_q: measurement height of humidity and isotopes in m
    dtstep: model timestep in s
    resolution: model domain vertical resolution in m (5mm)
    linear_deposition: True or False, if True empirical equations from Wahl et al. (2021) are used
    equilibrium_sublimation: True or False, if True, Sublimation flux isotopic composition is calculated assuming equilibrium fractionation
    VapExchange=True, if False: sublimation Flux isotopic composition = snow isotopic composition
    returnarray=True, return not only model uncertainty but also model result
    disturbance=True, if True, uncertainty is added to the input parameters, if false default values are used to obtain default model output in dayMC
    
    '''


    from numpy.random import default_rng
    rng=default_rng()
    #from pytictoc import TicToc #for calculating the time it takes to run the code
    #tictoc2=TicToc()
    #tictoc2.tic()
    Ls=2.834*1e6 #J/kg at 0°C
    kappa=0.4
   
    dayMC=np.zeros(shape=(runs,3))
    
    
    htrs=0.02
    k18O=6*1e-3
    kD=0.88*k18O
    # kD=XX. you can add a different kD.. if not specified kD=0.88*k18O
    
    
    # set up Crank-Nicolson Diffusion matrix    
    A=np.diag(np.ones(len(d18Osini)-1), k=-1)+np.diag(np.ones(len(d18Osini)-1), k=1) #create matrixes and add them with np.diag()
    np.fill_diagonal(A,-2) #is inplace=True
    
    #set upper boundary conditions
    A[0,0]=-1
    # lower boundary is slap that is not influenced by stuff going on above
    A[-1,-1]=0
    A[-1,-2]=0
    
    # set up sublimation matrix (only effecting 1 layer)
    B=np.zeros(len(d18Osini))
    B[0]=1
    
    
    for run in np.arange(runs):
    
    
        
        #randomize the input
        if disturbance==False: # if False, you simply calculate the default model results
            
            d18Osini_D=d18Osini
            dDsini_D=dDsini
            dexsini_D=dDsini_D-8*d18Osini_D           
            LE=LEini
            vd18O=vd18Oini
            vdD=vdDini            
            q=qini         
            
        else: #this is the case for calculating the model uncertainty due to uncertainty in the input parameters
            # the intitial profile is randomly disturbed in all the layers equally 
            d18Osini_D=d18Osini+rng.normal(loc=0, scale=0.53, size=len(d18Osini)) #0.53 is how good the mean is from the true mean, see Fig S5 Supplementary Material
            dDsini_D=dDsini+rng.normal(loc=0, scale=3.8, size=len(dDsini)) #3.8 is how good the mean is from the true mean, see Fig S5 Supplementary Material
            dexsini_D=dDsini_D-8*d18Osini_D   
            
            LE=LEini*rng.normal(loc=1, scale=0.2, size=len(LEini)) #20% error
    
            vd18O=vd18Oini+rng.normal(loc=0, scale=0.23, size=len(vd18Oini)) # Steen-Larsen et al. 2013
            vdD=vdDini+rng.normal(loc=0, scale=1.4, size=len(vdDini)) # Steen-Larsen et al. 2013
    
            q=qini+rng.normal(loc=0, scale=0.002*np.mean(qini), size=len(qini)) # Wahl et al 2021
    
            
        # start calculating h
        z0q=calc_z0q(z0m=z0,u_star=ustar)

        # calculate correction functions 
        psim,psiq=calc_psi(zl,y=15)

        # calculate qsat and h at surface from bulk estimates and q measurements 
        qsat=q+ LE/(rho_a*Ls*1e-3*ustar*kappa) * (np.log(z_q/z0q)-psiq)
        h=q/qsat
        #adjust h: # linear_deposition=True needs to be done after the randomization in case LE changes sign
        if linear_deposition:
            h[LE<=0] = 1 #use linear dependency for deposition
            
        if equilibrium_sublimation:
            h[LE>0] = 1 #use only equilibrium (no dependency on vapor)
            
        if VapExchange==0: 
            h[LE>0] = 0 #no fractionation during sublimation
            
        # calculate from the q_sat the respective surface Temperature
        vmr=ty.specific_humidity2vmr(qsat/1000) #kg
        Pw_sat=vmr*Pa #saturation vapor pressure at surface
        Ts=f_gg_inv_ice(Pw_sat)
        

        dz=resolution # 5mm in m
        H=5e-2 # 5cm in m
        dxi=dz/H # dimensionless height parameter 
        
        # define the snow isotope profile that will evolve
        d18Os=d18Osini_D.copy()
        dDs=dDsini_D.copy()
        dexs=dexsini_D.copy()    
    
    
        for t in np.arange(len(LE)):
            
            #need to define
            Tsnow=TempS[t]
            P_hPa=Pa[t]/100 #hPascall
            sr=LE[t]/Ls
            m=dz*rho_s #weight of first layer
            
            # calculate diffusivities (at least T changes with time)
            omega_18O,omega_D=calc_omegafirn(T=Tsnow,P=P_hPa,rho_s=rho_s)
    
            #'''
            # 1. step: calculate delta value of flux (dependent if condensation or sublimation)
            if LE[t]>0: #sublimation
                if h[t]==1: #equilibrium fractionation
                    delta_F18O = ((d18Os[0]*1e-3+1)*(1/alpha18O_Maj(Ts[t]))-1)*1e3
                    delta_FD   = ((dDs[0]*1e-3+1)*(1/alphaD_MN(Ts[t]))-1)*1e3                
                elif (h[t]<1)&(h[t]>0)&(abs(h[t]-1)>htrs): # kinetic effects                
                    delta_F18O = deltaF(h=h[t],k=k18O,delta_v=vd18O[t],delta_s=d18Os[0],Ts=Ts[t])
                    delta_FD   = deltaF(h=h[t],k=kD,delta_v=vdD[t],delta_s=dDs[0],Ts=Ts[t])
                elif (abs(h[t]-1)<htrs) & (abs(h[t]-1)>0): #h within threshold  
                    delta_F18O = d18Os[0]
                    delta_FD   = dDs[0]         
                elif h[t]==0:
                    delta_F18O = d18Os[0]
                    delta_FD   = dDs[0]
                    
                elif h[t]>1:
                    print('sublimation but RHs > 1: '+str(LE[t]))
                    break
                else:
                    print('Sublimation and h is nan for t='+str(t))
                    break                        
            else: #deposition
                if h[t]==1: #linear equations from Wahl et al. 2021
                    delta_F18O = (1.5)*vd18O[t]+23.78 # empirical 
                    delta_FD   = (2.24)*vdD[t]+445.20 #empirical 
                elif h[t]>1: # supersaturation
                    print('supersaturation: '+str(LE[t]))
                    break
                elif h[t]<1:
                    print('deposition but RHs < 1: '+str(LE[t]))
                    break
                else:
                    print('Deposition and I guess h is nan for t='+str(t))       
                    break    
            d18Os=d18Os + omega_18O/(H**2)*1/(dxi**2)*np.dot(A,d18Os)*dtstep + sr/m*np.dot((d18Os[0]-delta_F18O),B)*dtstep
            dDs=dDs + omega_D/(H**2)*1/(dxi**2)*np.dot(A,dDs)*dtstep + sr/m*np.dot((dDs[0]-delta_FD),B)*dtstep
            dexs=dDs-8*d18Os

            #update grid
            H=H-((sr/rho_s)*dtstep) 
            dz=H/len(d18Os)
            dxi=dz/H
                
        
        # calculate the difference to the ini profile and save in dayMC for this run
        Deltad18Os=d18Os[0]-d18Osini_D[0]# these are the distrubed intitial values, so only the Delta is calculated
        DeltadDs=dDs[0]-dDsini_D[0]
        Deltadexs=dexs[0]-dexsini_D[0]
        
        dayMC[run,:]=np.array([Deltad18Os, DeltadDs, Deltadexs])        
    #tictoc2.toc(msg='runs done')
    STDdayMC=np.std(dayMC,axis=0)
    
    # stdDeltad18Os=STDdayMC[0]
    # stdDeltadDs=STDdayMC[1]
    # stdDeltadexs=STDdayMC[2]  

    if returnarray==True:
        return STDdayMC,dayMC
    else:
        return STDdayMC
    
    

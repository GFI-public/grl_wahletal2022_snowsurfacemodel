#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 23 15:28:13 2022


Run snow surface model as published in Wahl et al. 2122
Geophysical Research Letters

This code can be used to generate the model results as well as the model uncertainty

@author: sonjaw
"""

import pandas as pd
import numpy as np
import datetime as datetime
import pickle
from pytictoc import TicToc

import smf_publish #snow surface model model functions

# define constants
sinday=24*60*60 #s seconds in day
RVSMOW_18O=2005.2*1e-6 #+- 0.45
RVSMOW_D=155.76*1e-6   # +-0.05  
rho_w=1000  #kg/m3 density of water
D_DD=0.9755   # Diffusivity ratio from Merlivat 1978
D_D18O=0.9723 # Diffusivity ratio from Merlivat 1978
Ls=2.834*1e6 #J/kg at 0°C, sublimation heat constant

### ----------------------------   Settings  ----------------------------  
interpolate=True
equilibrium_sublimation=False
vapex=True #it should be True if equilibrium_sublimation=True
diffusion=True  
disturbance=False #if False, the default result are given and the stdDelta dataframe will show 0 variability, if True, the uncertainty of each model day result is calculated    


### ----------------------------   Observational Data  ----------------------------  

### ADAPT THIS CODE TO YOUR OWN OBSERVATIONS
# the model input parameters needed are specified in function smf_publish.MCruns

# #load observational data
# # dfST is snow surface observations of isotopic composition
# dfST,dfIRGA,df2m,df7m,SrhoS,dfsnowT=smf_publish.load_EGRIP(interpolateVaporto30min=True)



# # define the input dataframe 
# #met
# LE=dfIRGA.LE.copy()
# u_star=dfIRGA.USTAR.copy()
# zL=dfIRGA.ZL.copy()
# Ls=Ls              #can be float
# Pa=dfIRGA.PA*1e3                #in Pascall
# rhoA=dfIRGA.amb_rho_a              #ambient moist air density
# rhoS=SrhoS              #snow density
# z0=1.3e-4
# WS=dfIRGA.WS.copy()
# WSmax=dfIRGA.WS_MAX.copy()


# tempS=dfsnowT.copy() # observations 10cm

# #Picarro
# q=df2m.qPIC.copy()               #picarro humidity in specific humidity g/kg
# z_q=2               #measurement height q,d18O,dD
# d18O=df2m.d18O.copy()
# dD=df2m.dD.copy()

#process input
#dfinput=smf_publish.define_input(LE=LE,u_star=u_star,ZL=zL,Ls=Ls,PA=Pa,q=q,z_q=z_q,deltaV_d18O=d18O,deltaV_dD=dD,rhoA=rhoA,rhoS=rhoS,tempS=tempS,WS=WS,WSmax=WSmax,z0=z0,interpolate=interpolate)


### ----------------------------   Snow Surface Model  ----------------------------  


# create dataframe with index daterange of days for which model sohuld be run
dfMC=pd.DataFrame(index=dfST.index,columns=['stdDeltad18O','stdDeltadD','stdDeltadex'])

#idefine dataframe to get the actual results and not only the uncertainty range for the individual days
dfmodel_results=pd.DataFrame(index=dfST.index,columns=['d18O','dD','dex'])

tictoc=TicToc()
tictoc.tic()
tictoc2=TicToc()
tictoc2.tic()
for i,day in enumerate(dfST.index[:]): # index must have hour and minute information
    #prepare input of this one day

    start=day #starting day including time information
    end=dfST.index[i+1]-datetime.timedelta(minutes=30) #
    
    # snow domain resolution
    resolution=5*1e-3 #m
    # time resolution, timestep
    dt=(dfinput.index[1]-dfinput.index[0]).seconds #30min in sec
    
    
    # define all timeseries as arrays. Timeseries must have length of modeling period with resolution dt
    vd18O=dfinput.loc[start:end,'d18O'].to_numpy() #vapor isotopic composition d18O in ‰
    vdD=dfinput.loc[start:end,'dD'].to_numpy() #vapor isotopic composition dD in ‰
    q=dfinput.loc[start:end,'q'].to_numpy() #ambient humidity
    TempS=dfinput.loc[start:end,'tempS'].to_numpy() #snow temperature °C
    rho_s=dfinput.loc[start,'rhoS'] #snow density kg m-3
    Pa=dfinput.loc[start:end,'PA'].to_numpy()   # ambient pressure
    ustar=dfinput.loc[start:end,'u_star'].to_numpy() #friction velocity
    z0=dfinput.loc[start:end,'z0'].to_numpy() #roughness length in m
    ZL=dfinput.loc[start:end,'ZL'].to_numpy() #stability parameter
    rho_A=dfinput.loc[start:end,'rhoA'].to_numpy() #air density
    
    LE=dfinput.loc[start:end,'LE'].to_numpy() # latent heat flux Wm-2
    

    d18Osini=smf_publish.snow_profile_ini(day,dfST=dfST,resolution=resolution,season=day.year)[0] #populate initial snow model domain
    dDsini=smf_publish.snow_profile_ini(day,dfST=dfST,resolution=resolution,season=day.year)[1]  #populate initial snow model domain
    dexsini=dDsini-8*d18Osini
    
    # h and Ts are caluclated in the MCruns 
    # specify runs if you want to calculate the uncertainty of the model results
    stdDelta,dayMC=smf_publish.MCruns(runs=1,d18Osini=d18Osini,dDsini=dDsini,dexsini=dexsini,LEini=LE,vd18Oini=vd18O,vdDini=vdD,qini=q,ustar=ustar,zl=ZL,TempS=TempS,Pa=Pa,rho_s=rho_s,rho_a=rho_A,z0=z0,z_q=z_q,dt=dt,resolution=resolution,equilibrium_sublimation=equilibrium_sublimation,linear_deposition=True,VapExchange=vapex,returnarray=True,disturbance=disturbance,diffusion=diffusion)
    
    dfMC.iloc[i+1,:]=[stdDelta[0],stdDelta[1],stdDelta[2]]
    dfmodel_results.iloc[i+1,:]=dayMC[0,:]# [Deltad18Os, DeltadDs, Deltadexs]

    tictoc.toc(msg='day done '+str(day),restart=True)
#with open('path/to/output_folder/dfMC.pkl', 'wb') as f: #path to model uncertainty output
#    pickle.dump(dfMC, f) 
dfmodel_results.index=dfmodel_results.index.normalize() #removes the time information
#with open('path/to/output_folder.pkl', 'wb') as f:  # path to model output
#    pickle.dump(dfmodel_results, f) 

tictoc2.toc(msg='computing done ')   
